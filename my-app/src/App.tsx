import React, { Fragment, useState, useEffect } from "react";
import axios from "axios";
import "./App.css";
import JokeCard from "./components/JokeCard/JokeCard";

function App() {
  const [joke, jokeHandler] = useState<string>("");
  const [imageFlag, imageHandler] = useState<boolean>(true);

  useEffect(() => {
    const api = axios.create({ baseURL: "https://api.icndb.com/jokes/random" });
    const drawJoke = () => {
      api
        .get("/")
        .then((res) => {
          var joke: string = res.data.value.joke;
          var jokeQuote: string = joke.replace(/&quot;/g, '"');
          jokeHandler(jokeQuote);
        })
        .catch((error) => {
          jokeHandler("Something went wrong, sorry!");
        });
    };
    drawJoke();
  }, []);
  return (
    <div className="App">
      <Fragment>
        <JokeCard
          joke={joke}
          imageFlag={imageFlag}
          jokeHandler={jokeHandler}
          imageHandler={imageHandler}
        />
      </Fragment>
    </div>
  );
}

export default App;
