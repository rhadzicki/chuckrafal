import classes from "./JokeContainer.module.scss";
const Joke: React.FC<{ joke: string }> = (props) => {
  return <div className={classes.container}>{props.joke}</div>;
};
export default Joke;
