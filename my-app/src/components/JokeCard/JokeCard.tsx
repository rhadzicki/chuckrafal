import Card from "../UI/Card";

import Joke from "./Joke";
import JokeForm from "./JokeForm";
import JokeImage from "./Image";

const JokeCard: React.FC<{
  joke: string;
  imageFlag: boolean;
  jokeHandler: (joke: string) => void;
  imageHandler: (flag: boolean) => void;
}> = (props) => {
  return (
    <Card>
      <JokeImage imageFlag={props.imageFlag} />
      <Joke joke={props.joke} />
      <JokeForm
        jokeHandler={props.jokeHandler}
        imageHandler={props.imageHandler}
      />
    </Card>
  );
};
export default JokeCard;
