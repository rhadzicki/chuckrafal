import { Fragment } from "react";
import Chuck from "../assets/chuck-norris-photo.png";
import RandomPerson from "../assets/Random-photo.png";
import classes from "./Image.module.scss";
const JokeImage: React.FC<{ imageFlag: boolean }> = (props) => {
  return (
    <Fragment>
      <img
        className={classes.image}
        src={props.imageFlag ? Chuck : RandomPerson}
        alt="Empty!"
      />
    </Fragment>
  );
};
export default JokeImage;
