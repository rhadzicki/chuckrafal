import React, { Fragment, useRef, useState } from "react";
import classes from "./JokeForm.module.scss";
import axios from "axios";
import SaveJoke from "./SaveJoke";

const JokeForm: React.FC<{
  jokeHandler: (joke: string) => void;
  imageHandler: (flag: boolean) => void;
}> = (props) => {
  const [category, categoryHandler] = useState<string>('""');
  const textInput = useRef<HTMLInputElement>(null);
  const categoryInput = useRef<HTMLSelectElement>(null);
  const [Name, changeName] = useState<string>("Chuck");
  const [SurName, changeSurName] = useState<string>("Norris");

  const changeCategory = () => {
    categoryHandler(categoryInput.current!.value);
  };

  const splitName = () => {
    const name = textInput.current!.value;
    if (name.length >= 1) {
      const result = name.trim().split(" ");

      if (result.length === 1 && result) {
        changeName(result[0]);
        changeSurName("");
      } else if (result.length >= 2) {
        changeName(result[0]);
        changeSurName(result[1]);
      }
    } else {
      changeName("Chuck");
      changeSurName("Norris");
    }
  };

  const submitForm = (e: React.FormEvent) => {
    e.preventDefault();
    const URL1 = `https://api.icndb.com/jokes/random?firstName=${Name}&lastName=${SurName}&limitTo=${category}`;
    const api = axios.create({ baseURL: URL1 });
    textInput.current!.value.length > 1
      ? props.imageHandler(false)
      : props.imageHandler(true);
    const drawJoke = () => {
      api
        .get("")
        .then((res) => {
          var joke: string = res.data.value.joke;
          var jokeQuote: string = joke.replace(/&quot;/g, '"');
          props.jokeHandler(jokeQuote);
        })
        .catch((error) => {
          props.jokeHandler("Something went wrong, sorry!");
        });
    };

    drawJoke();
  };
  return (
    <Fragment>
      <form onSubmit={submitForm}>
        <select
          className={classes.select}
          onChange={changeCategory}
          ref={categoryInput}
        >
          <option value='""'>No category</option>
          <option value="nerdy">Nerdy</option>
          <option value="explicit">Explicit</option>
        </select>
        <input
          placeholder="Impersonate Chuck"
          className={classes.input}
          ref={textInput}
          onChange={splitName}
        />
        <button className={classes.draw_joke_button}>
          Draw a random {Name} {SurName} joke
        </button>
      </form>
      <SaveJoke
        Name={Name}
        Surname={SurName}
        category={category}
        JokeHandler={props.jokeHandler}
      />
    </Fragment>
  );
};
export default JokeForm;
