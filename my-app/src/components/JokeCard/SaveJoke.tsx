import { useRef, useState } from "react";
import axios from "axios";
import classes from "./SaveJoke.module.scss";
const SaveJoke: React.FC<{
  JokeHandler: (joke: string) => void;
  Name: string;
  Surname: string;
  category: string;
}> = (props) => {
  const [counter, setCounter] = useState<number>(0);
  const saveInput = useRef<HTMLInputElement>(null);
  const increment = () => {
    const incrCounter = counter + 1;
    setCounter(incrCounter);
  };
  const updateCounter = () => {
    const newValue: number = +saveInput.current!.value;
    setCounter(newValue);
  };
  const decrement = () => {
    if (counter > 0) {
      const decCounter = counter - 1;
      setCounter(decCounter);
    }
  };
  const saveJokes = () => {
    if (counter > 0) {
      const URL1 = `https://api.icndb.com/jokes/random/${counter}?firstName=${
        props.Name
      }&lastName=${props.Surname}${
        props.category.length > 2 ? `&limitTo=${props.category}` : ""
      }`;
      const api = axios.create({ baseURL: URL1 });
      api
        .get("")
        .then((res) => {
          const data1 = res.data.value;
          let jokes = "";
          for (let i = 0; i < data1.length; i++) {
            const quote = data1[i].joke;
            jokes = jokes.concat("\n\n", quote);
          }
          jokes = jokes.replace(/&quot;/g, "'");
          let a = document.createElement("a");
          let blob = new Blob([jokes], {
            type: "text/plain",
          });
          a.href = URL.createObjectURL(blob);
          a.download = "jokes";

          a.click();
        })
        .catch((error) => {
          props.JokeHandler("Something went wrong, sorry!");
        });
    }
  };
  return (
    <div>
      <div className={classes.buttons_container}>
        <div
          className={`${classes.button_container} ${
            counter > 100 && classes.button_container_error
          }`}
        >
          <button className={classes.minus} onClick={decrement} />
          <input
            type="text"
            value={counter}
            className={classes.indicator}
            ref={saveInput}
            onChange={updateCounter}
          ></input>
          <button className={classes.plus} onClick={increment} />
        </div>
        <button
          className={
            counter > 0 && counter < 101
              ? classes.save_button_active
              : classes.save_button
          }
          onClick={saveJokes}
          disabled={counter < 1 || counter > 100 ? true : false}
        >
          Save Jokes
        </button>
      </div>
      {counter > 100 ? (
        <p className={classes.save_error_text}>
          You can pick a number from 1 to 100.
        </p>
      ) : null}
    </div>
  );
};
export default SaveJoke;
