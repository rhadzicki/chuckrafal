import classes from "./Card.module.scss";
const Card: React.FC = (props) => {
  return (
    <div className={classes.CardContainer}>
      <div className={classes.Card}>{props.children}</div>
    </div>
  );
};
export default Card;
