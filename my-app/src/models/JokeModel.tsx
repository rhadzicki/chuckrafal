class JokeModel {
  id: number;
  category?: string;
  firstName?: string;
  lastName?: string;
  Text: string;
  constructor(
    id:number,
    jokeCategory: string,
    userFirstName: string,
    userLastName: string,
    jokeText: string
  ) {
    this.id = id
    this.category = jokeCategory;
    this.firstName = userFirstName;
    this.lastName = userLastName;
    this.Text = jokeText;
  }
}
export default JokeModel;
